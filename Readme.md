Collection of plugins and tweaks for TW5. One source of truth.

Table of Contents
=================

   * [Plugins](#plugins)


### Plugins

* Action-selection: Create tiddlers with current selection as the tiddler-text.
* Action-mark: Surround the selected text with any html element
* Snapshot: Create timestamped Backups